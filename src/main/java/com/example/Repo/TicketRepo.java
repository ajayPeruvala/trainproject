package com.example.Repo;

import org.springframework.data.repository.CrudRepository;

import com.example.Model.Ticket;

public interface TicketRepo extends CrudRepository<Ticket, Long>{

}
