package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.example.Controller.TrainController;

@EnableJpaRepositories("com.example.Repo")
@EntityScan("com.example.Model")
@ComponentScan(basePackageClasses=TrainController.class)
@SpringBootApplication
public class TrainProjectFinalApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrainProjectFinalApplication.class, args);
	}

}
