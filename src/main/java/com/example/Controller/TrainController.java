package com.example.Controller;


import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.example.Dao.CustomerDao;
import com.example.Dao.TicketDao;
import com.example.Dao.TrainDao;
import com.example.Model.Customer;
import com.example.Model.Ticket;
import com.example.Model.Train;
import com.example.Service.CustomerService;
import com.example.Service.TicketService;

@ComponentScan("com.example.Dao")
@ComponentScan("com.example.Service")
@RestController
public class TrainController {
	
	@Autowired
	CustomerDao Custdao;
	
	@Autowired
	CustomerService CustSer;
	
	@Autowired
	TrainDao trainDao;
	
	@Autowired
	TicketDao ticketDao;
	
	@Autowired
	TicketService ticketSer;
	
	private HttpSession session;
	
	@RequestMapping("/")
	public ModelAndView home(ModelAndView model)
	{
		model.setViewName("home");
		return model;
	}
	
	@PostMapping("/action")
	public ModelAndView doPost(ModelAndView model, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
	{
		response.setContentType("txt/html");
		String CustName = request.getParameter("custName");
		String CustPassword = request.getParameter("custPassword");
		int CustAge = Integer.parseInt(request.getParameter("custAge"));
		String CustEmail = request.getParameter("custEmail");
		String CustPhoneNumber = request.getParameter("custPhone");
		String CustAddress = request.getParameter("custAddress");
		Customer c = CustSer.getCustomer(CustName, CustPassword, CustAge, CustEmail, CustPhoneNumber, CustAddress);
		Custdao.save(c);
		model.setViewName("CustomerSuccessful");
		return model;
	}
	@RequestMapping("/login")
	public ModelAndView login(ModelAndView model)
	{
		model.setViewName("Login");
		return model;
	}
	
	@RequestMapping("/LoginSuccessful")
	public ModelAndView doGet(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
	{
		response.setContentType("text/html");
		String UserName = request.getParameter("txtUser");
		String Password = request.getParameter("txtPassword");
		Customer c = Custdao.getCustomer(UserName, Password);
		session = request.getSession();
		if(c != null)
		{
			session.setAttribute("name", UserName);
			session.setAttribute("email", c.getEmail());
			session.setAttribute("age", c.getAge());
			model.setViewName("LoginSuccessful");
		}
		else {
		model.setViewName("LoginFailed");}
		return model;
	}
	@RequestMapping("/searchTrain")
	public ModelAndView SearchTrain(ModelAndView model, HttpServletRequest request, HttpServletResponse response)
	{
		response.setContentType("text/html");
		String coachType = request.getParameter("txtCoach");
		session.setAttribute("coachType", coachType);
		String source = request.getParameter("txtSource");
		session.setAttribute("source", source);
		String destination = request.getParameter("txtDestination");
		session.setAttribute("destination", destination);
		List<Train> resultList = trainDao.searchTrain(coachType, source, destination);
		if(resultList.size() == 0)
		{
			model.setViewName("InvalidCoachType");
		}else if(resultList.get(0).getTno() == 0)
		{
			model.setViewName("NoTrainsFound");
		}else
		{
			model.setViewName("Trains");
			request.setAttribute("trainList", resultList);
		}
		return model;
	}
	
	@RequestMapping("/bookTicket")
	public ModelAndView bookTicket(ModelAndView model, HttpServletRequest request, HttpServletResponse response) throws ParseException
	{
		response.setContentType("text/html");
		String TrainName = (String)request.getParameter("submit");
		String source = (String) session.getAttribute("source");
		String destination = (String) session.getAttribute("destination");
		String passenger = (String) session.getAttribute("name");
		String email = (String) session.getAttribute("email");
		Date idate = new Date(System.currentTimeMillis());
		String Airdate  = request.getParameter("date");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date joudate = new Date(formatter.parse(Airdate).getTime());
		String ticketNumber = source.substring(0, 3) + passenger.substring(0,3) + joudate.toString().substring(joudate.toString().length()-2, joudate.toString().length()) + destination.substring(0, 3);
		Ticket t = ticketSer.getTicket(ticketNumber, TrainName, source, destination, idate, joudate, passenger, email);
		ticketDao.save(t);
		request.setAttribute("TicketDetails", t);
		model.setViewName("TicketView");
		return model;
	}
}
