package com.example.Dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.Model.Train;
import com.example.Repo.TrainRepo;
import com.example.Service.TrainService;


@Repository
public class TrainDao {
	
	@Autowired
	TrainRepo trainRepo;
	
	@Autowired
	TrainService trainService;
	
	List<Train> trainList;
	
	public void getTrainList()
	{
		trainList = (List<Train>) trainRepo.findAll();
	}
	
	public List<Train> searchTrain(String coachType, String source, String destination)
	{
		getTrainList();
		return trainService.search(trainList, coachType, source, destination);
	}
	
	
}
