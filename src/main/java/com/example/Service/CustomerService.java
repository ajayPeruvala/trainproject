package com.example.Service;

import org.springframework.stereotype.Service;

import com.example.Model.Customer;

@Service
public class CustomerService {
	public Customer getCustomer(String name, String password, int age, String email, String phone, String address)
	{
		Customer c = new Customer(name, password, age, email, phone, address);
		return c;
	}
}
