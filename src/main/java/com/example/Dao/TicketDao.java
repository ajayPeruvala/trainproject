package com.example.Dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.Model.Ticket;
import com.example.Repo.TicketRepo;

@Repository
public class TicketDao {
	
	@Autowired
	TicketRepo ticketRepo;
	
	public void save(Ticket t)
	{
		ticketRepo.save(t);
	}
}
