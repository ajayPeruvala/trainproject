package com.example.Repo;

import org.springframework.data.repository.CrudRepository;

import com.example.Model.Train;

public interface TrainRepo extends CrudRepository<Train, Long>{

}
