package com.example.Service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.Model.Train;

@Service
public class TrainService {
	
	List<Train> data;
	
	public int validateCoachType(String coachType)
	{
		if(coachType.equalsIgnoreCase("SLEEPER"))
		{
			return 0;
		}
		if(coachType.equalsIgnoreCase("SEATER"))
		{
			return 1;
		}
		if(coachType.equalsIgnoreCase("AC1"))
		{
			return 2;
		}
		if(coachType.equalsIgnoreCase("AC2"))
		{
			return 3;
		}
		if(coachType.equalsIgnoreCase("AC3"))
		{
			return 4;
		}
		return 5;
	}
	
	public List<Train> viewTrain(String coachType, String source, String destination)
	{
		List<Train> result_list = new ArrayList<Train>();
		Train t = new Train(0, "", "", "", 0, 0, 0, 0, 0);
		if(validateCoachType(coachType) == 0)
		{
			for(int i=0;i<data.size();i++)
			{
				Train train = data.get(i);
				if(train.getSo().equalsIgnoreCase(source) && train.getDest().equalsIgnoreCase(destination) && (train.getSleeper()>0))
				{
					//System.out.println(train.getTrainName());
					result_list.add(train);
				}
			}
			if(result_list.size() == 0)
			{
				result_list.add(t);
			}
			return result_list;
		}else if(validateCoachType(coachType) == 1)
		{
			for(int i=0;i<data.size();i++)
			{
				Train train = data.get(i);
				if(train.getSo().equalsIgnoreCase(source) && train.getDest().equalsIgnoreCase(destination) && (train.getSeater()>0))
				{
					//System.out.println(train.getTrainName());
					result_list.add(train);
				}
			}
			if(result_list.size() == 0)
			{
				result_list.add(t);
			}
			return result_list;
		}else if(validateCoachType(coachType) == 2)
		{
			for(int i=0;i<data.size();i++)
			{
				Train train = data.get(i);
				if(train.getSo().equalsIgnoreCase(source) && train.getDest().equalsIgnoreCase(destination) && (train.getAc1()>0))
				{
					//System.out.println(train.getTrainName());
					result_list.add(train);
				}
			}
			if(result_list.size() == 0)
			{
				result_list.add(t);
			}
			return result_list;
		}else if(validateCoachType(coachType) == 3)
		{
			for(int i=0;i<data.size();i++)
			{
				Train train = data.get(i);
				if(train.getSo().equalsIgnoreCase(source) && train.getDest().equalsIgnoreCase(destination) && (train.getAc2()>0))
				{
					//System.out.println(train.getTrainName());
					result_list.add(train);
				}
			}
			if(result_list.size() == 0)
			{
				result_list.add(t);
			}
			return result_list;
		}else if(validateCoachType(coachType) == 4)
		{
			for(int i=0;i<data.size();i++)
			{
				Train train = data.get(i);
				if(train.getSo().equalsIgnoreCase(source) && train.getDest().equalsIgnoreCase(destination) && (train.getAc3()>0))
				{
					//System.out.println(train.getTrainName());
					result_list.add(train);
				}
			}
			if(result_list.size() == 0)
			{
				result_list.add(t);
			}
			return result_list;
		}else
		{
			return result_list;
		}
	}
	
	
	public List<Train> search(List<Train> trainList, String coachType, String source, String destination)
	{
		data = trainList;
		List<Train> result_list = viewTrain(coachType, source, destination);
		return result_list;
	}
	

}
