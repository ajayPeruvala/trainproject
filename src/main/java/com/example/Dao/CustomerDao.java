package com.example.Dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.Model.Customer;
import com.example.Repo.CustomerRepo;

@Repository
public class CustomerDao {
	
	@Autowired
	CustomerRepo custRepo;
	
	List<Customer> customerList;
	
	public void save(Customer C)
	{
		custRepo.save(C);
	}
	
	public void getList()
	{
		customerList = (List<Customer>) custRepo.findAll();
	}
	
	public Customer getCustomer(String Username, String password)
	{
		getList();
		for(int i=0;i<customerList.size();i++)
		{
			if(customerList.get(i).getName().equalsIgnoreCase(Username) && customerList.get(i).getPassword().equals(password))
			{
				return customerList.get(i);
			}
		}
		return null;
	}
}
