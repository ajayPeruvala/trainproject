package com.example.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Trains")
public class Train {
	@Id
	private int tno;
	private String tname;
	private String so;
	private String  dest;
	private int ac1;
	private int ac2;
	private int ac3;
	private int sleeper;
	private int seater;
	/**
	 * @param tno
	 * @param tname
	 * @param so
	 * @param dest
	 * @param ac1
	 * @param ac2
	 * @param ac3
	 * @param sleeper
	 * @param seater
	 */
	public Train() {}
	public Train(int tno, String tname, String so, String dest, int ac1, int ac2, int ac3, int sleeper, int seater) {
		super();
		this.tno = tno;
		this.tname = tname;
		this.so = so;
		this.dest = dest;
		this.ac1 = ac1;
		this.ac2 = ac2;
		this.ac3 = ac3;
		this.sleeper = sleeper;
		this.seater = seater;
	}
	public int getTno() {
		return tno;
	}
	public void setTno(int tno) {
		this.tno = tno;
	}
	public String getTname() {
		return tname;
	}
	public void setTname(String tname) {
		this.tname = tname;
	}
	public String getSo() {
		return so;
	}
	public void setSo(String so) {
		this.so = so;
	}
	public String getDest() {
		return dest;
	}
	public void setDest(String dest) {
		this.dest = dest;
	}
	public int getAc1() {
		return ac1;
	}
	public void setAc1(int ac1) {
		this.ac1 = ac1;
	}
	public int getAc2() {
		return ac2;
	}
	public void setAc2(int ac2) {
		this.ac2 = ac2;
	}
	public int getAc3() {
		return ac3;
	}
	public void setAc3(int ac3) {
		this.ac3 = ac3;
	}
	public int getSleeper() {
		return sleeper;
	}
	public void setSleeper(int sleeper) {
		this.sleeper = sleeper;
	}
	public int getSeater() {
		return seater;
	}
	public void setSeater(int seater) {
		this.seater = seater;
	}
	@Override
	public String toString() {
		return "Train [tno=" + tno + ", tname=" + tname + ", so=" + so + ", dest=" + dest + ", ac1=" + ac1 + ", ac2="
				+ ac2 + ", ac3=" + ac3 + ", sleeper=" + sleeper + ", seater=" + seater + "]";
	}
	
}
