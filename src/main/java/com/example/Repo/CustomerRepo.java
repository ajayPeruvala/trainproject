package com.example.Repo;

import org.springframework.data.repository.CrudRepository;

import com.example.Model.Customer;

public interface CustomerRepo extends CrudRepository<Customer, Long>{

}
