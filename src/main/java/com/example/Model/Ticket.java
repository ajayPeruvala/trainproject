package com.example.Model;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tickets")
public class Ticket {
	@Id
	private String tid;
	private String train;
	private String source;
	private String destination;
	private Date idate;
	private Date joudate;
	private String passenger;
	private String email;
	private int tstatus;
	/**
	 * @param tid
	 * @param train
	 * @param source
	 * @param destination
	 * @param idate
	 * @param passenger
	 * @param tstatus
	 */
	 public Ticket() {}
	public Ticket(String tid, String train, String source, String destination, Date idate, Date joudate, String passenger, String email) {
		super();
		this.tid = tid;
		this.train = train;
		this.source = source;
		this.destination = destination;
		this.idate = idate;
		this.joudate = joudate;
		this.passenger = passenger;
		this.email = email;
		this.tstatus = 1;
	}
	public String getTid() {
		return tid;
	}
	public void setTid(String tid) {
		this.tid = tid;
	}
	public String getTrain() {
		return train;
	}
	public void setTrain(String train) {
		this.train = train;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public Date getIdate() {
		return idate;
	}
	public void setIdate(Date idate) {
		this.idate = idate;
	}
	
	public Date getJoudate() {
		return joudate;
	}
	
	public void setJoudate(Date joudate) {
		this.joudate = joudate;
	}
	public String getPassenger() {
		return passenger;
	}
	public void setPassenger(String passenger) {
		this.passenger = passenger;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}
	public int getTstatus() {
		return tstatus;
	}
	public void setTstatus(int tstatus) {
		this.tstatus = tstatus;
	}
	
	
	@Override
	public String toString() {
		return "Ticket [tid=" + tid + ", train=" + train + ", source=" + source + ", destination=" + destination
				+ ", idate=" + idate + ", joudate=" + joudate + ", passenger=" + passenger + ", tstatus=" + tstatus + "]";
	}
	
	
}
