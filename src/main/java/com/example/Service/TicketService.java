package com.example.Service;

import java.sql.Date;

import org.springframework.stereotype.Service;

import com.example.Model.Ticket;

@Service
public class TicketService {

	public Ticket getTicket(String tid, String tname, String source, String destination, Date idate, Date joudate, String passenger, String email)
	{
		Ticket t = new Ticket(tid, tname, source, destination, idate, joudate, passenger, email);
		return t;
	}
}	
