package com.example.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="Customers")
public class Customer {
	@Id
	private String name;
	private String password;
	private int age;
	private String email;
	private String phone;
	private String address;
	/**
	 * @param name
	 * @param password
	 * @param age
	 * @param email
	 * @param phoneNumber
	 * @param address
	 */
	public Customer() {}
	public Customer(String name, String password, int age, String email, String phoneNumber, String address) {
		super();
		this.name = name;
		this.password = password;
		this.age = age;
		this.email = email;
		this.phone = phoneNumber;
		this.address = address;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phoneNumber) {
		this.phone = phoneNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	@Override
	public String toString() {
		return "Customer [name=" + name + ", password=" + password + ", age=" + age + ", email=" + email
				+ ", phoneNumber=" + phone + ", address=" + address + "]";
	}
	
	
}
